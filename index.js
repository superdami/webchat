var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var cookieSession = require('cookie-session')

var loginCheck = function(req, res, next) {
    if(req.session.user){
      next();
    }else{
      res.redirect('/lobby');
    }
};

app.use(express.static(__dirname + '/public'));
app.use(bodyParser());
app.set('views', __dirname + '/views');
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);
app.use(cookieSession({
    secret: 'secret',
    cookie: {
        httpOnly: false,
        maxAge: new Date(Date.now() + 60 * 60 * 1000)
    }
}));
app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'));
var io = require('socket.io').listen(server);
var userNames = [];
var userInRooms = {};

io.sockets.on('connection', function (socket) {
    socket.on('subscribe', function(room, userName){
        socket.userName = userName;
        socket.room = room;
    	socket.join(room);

        if(userInRooms[room]) {
            userInRooms[room].push(userName);
        }
        else {
            userInRooms[room] = [userName];
        }

        socket.broadcast.to(room).emit('message', {userName:"server", message:socket.userName + ' has connected to this room'});
        socket.in(socket.room).emit('message', {userName:"server", message:"you have connected to this room"});
    });

    socket.on('unsubscribe', function(room, userName){
    	socket.leave(room);
        userInRooms[room].remove(userName);
        socket.broadcast.to(room).emit('message', {userName:"server", message:socket.userName + ' has left this room'});
    });

    socket.on('message', function(data) {
    	io.sockets.in(socket.room).emit('message', {userName:socket.userName, message:data});
    });

    socket.on('image', function (data) {
        io.sockets.in(socket.room).emit('image', {userName:socket.userName, image:data});
    })
});

function getRoomNames() {
    var rooms = io.sockets.manager.rooms;
    var newRooms = [];
    for (var roomName in rooms) {
        if (roomName.length) {
            name = roomName.substring(1);
            newRooms.push(name);
        }
    }

    return newRooms;
}

app.get("/lobby", function(req, res) {
    var roomNames = getRoomNames();
    console.log("loaded rooms", roomNames);
    res.render("lobby", {roomNames: JSON.stringify(roomNames)});
});

app.get('/room/:room/:userName', function(req, res) {
    var room = req.params.room;
    var userName = req.params.userName;
    console.log(req.params);

    res.render("chat", {port: app.get('port'), userName: userName, room: room});
});

app.post("/addUser", function(req, res){
    var user = req.body.userName;
    console.log("try to login user ", user);
    if (userNames.indexOf(user) >= 0) {
        res.send({login:false, message:"user existed"});
    }
    else {
        userNames.push(user);
        req.session.user = user;
        res.send({login:true, message:"login successful"});
    }
})

app.get('/createRoom', function(req, res) {
    var roomName = req.query.roomName;
    var userName = req.query.userName;

    var roomNames = getRoomNames();
    for (var index in roomNames) {
        if(roomNames[index] === roomName) {
            res.send({created:false, message:"room existed"});
            return;
        }
    }

    var url = '/room/' + roomName + "/" + userName; 
    res.send({created:true, url:url});
})

app.get('/joinRoom', function(req, res) {
    var roomName = req.query.roomName;
    var userName = req.query.userName;

    var roomNames = getRoomNames();

    var url;
    var message;
    if (roomNames.indexOf(roomName) >= 0) {
        url = '/room/' + roomName + "/" + userName; 
    }
    else {
        message = "room not existed";
    }
    res.send({url:url, message:message});
})

